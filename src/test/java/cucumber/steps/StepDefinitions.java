package cucumber.steps;

import cucumber.context.ContextVariable;
import cucumber.context.HttpCallContext;
import cucumber.utils.JSONUtils;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class StepDefinitions {
    private final HttpCallContext httpCallContext;
    private final ContextVariable contextVariable;

    public StepDefinitions(HttpCallContext httpCallContext, ContextVariable contextVariable) {
        this.httpCallContext = httpCallContext;
        this.contextVariable = contextVariable;
    }

    @Given("Create a tourist with json {string}")
    public void createATourist(String jsonPath) throws IOException {
        String json = JSONUtils.readJsonFileToString(jsonPath);

        String body = contextVariable.buildJsonWithContext(json);

        httpCallContext.setHttpCallResponse(given()
                .body(body)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON).when()
                .post("http://restapi.adequateshop.com/api/Tourist"));

        assert (HttpStatus.SC_CREATED == httpCallContext.getHttpCallResponse().getStatusCode());
    }

    @When("Update a tourist with {string} and json {string}")
    public void updateATourist(String id, String jsonPath) throws IOException {
        String json = JSONUtils.readJsonFileToString(jsonPath);

        String body = contextVariable.buildJsonWithContext(json);

        String idVariable = contextVariable.getVariableValue(id);

        httpCallContext.setHttpCallResponse(given()
                .body(body)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON).when()
                .put("http://restapi.adequateshop.com/api/Tourist/" + idVariable));
    }

    @When("Get a tourist by id {string}")
    public void getATourist(String id) {
        String idVariable = contextVariable.getVariableValue(id);

        httpCallContext.setHttpCallResponse(given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON).when()
                .get("http://restapi.adequateshop.com/api/Tourist/" + idVariable));
    }

}
