package cucumber;

import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SpringBootTestLoader {
    @BeforeAll
    public static void init() {
//        ContainersWrapped.getInstance();
    }

    @After
    public static void afterScenario() {
//        ContainersWrapped.getInstance().databasePostgreSQLCleanup();
    }

    @AfterAll
    public static void stop() {
//        ContainersWrapped.getInstance().techDatabaseLogs();
    }
}
