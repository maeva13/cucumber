package cucumber.steps;

import cucumber.context.ContextVariable;
import cucumber.context.HttpCallContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class CommonStep {

    private final HttpCallContext httpCallContext;
    private final ContextVariable contextVariable;

    public CommonStep(HttpCallContext httpCallContext, ContextVariable contextVariable) {
        this.httpCallContext = httpCallContext;
        this.contextVariable = contextVariable;
    }

    @Given("Generating a custom email into variable {string}")
    public void generatingACustomEmail(String variableName) {
        String strings = "abcdefghiklmnopqrstuvwxyz0123456789"; //liste des caractères possibles
        int s_length = 10; //taille de la chaîne de caractères souhaitée
        StringBuilder mailCustom = new StringBuilder();
        for (int i = 0; i < s_length; i++) {
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(strings.length());
            mailCustom.append(strings.charAt(randomInt));
        }
        contextVariable.buildContextVariable(Map.of(variableName, "email.client.22+test_" + mailCustom + "@gmail.com"));
    }

    @And("Save variables")
    public void andISaveInVariables(DataTable variables) throws JSONException {
        List<Map<String, String>> rows = variables.asMaps(String.class, String.class);

        JSONObject json = new JSONObject(httpCallContext.getHttpCallResponse().getBody().asString());

        for (Map<String, String> columns : rows) {
            String value = null;
            try {
                value = json.getString(columns.get("variableToSave"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (value != null) {
                contextVariable.buildContextVariable(Map.of(columns.get("nameOfVariableRegistered"), value));
            }
        }
    }

    @Then("A 204 response is received")
    public void iReceiveA204Response() {
        assert (HttpStatus.SC_NO_CONTENT == httpCallContext.getHttpCallResponse().getStatusCode());
    }

    @Then("A 201 response is received")
    public void iReceiveA201Response() {
        assert (HttpStatus.SC_CREATED == httpCallContext.getHttpCallResponse().getStatusCode());
    }

    @Then("A 200 response is received")
    public void iReceiveA200Response() {
        assert (HttpStatus.SC_OK == httpCallContext.getHttpCallResponse().getStatusCode());
    }
}
