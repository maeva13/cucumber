Feature: An example

#  Scenario: Update a tourist information
#    Given Generating a custom email into variable "email"
#    And Create a tourist with json "createTourist.json"
#    And Save variables
#      | variableToSave | nameOfVariableRegistered |
#      | id             | id                       |
#    When Update a tourist with "${id}" and json "updateTourist.json"
#    Then A 204 response is received

  Scenario: Get a tourist information
    Given Generating a custom email into variable "email"
    And Create a tourist with json "createTourist.json"
    And Save variables
      | variableToSave | nameOfVariableRegistered |
      | id             | id                       |
    When Get a tourist by id "${id}"
    Then A 200 response is received