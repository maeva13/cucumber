package cucumber.context;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContextVariable {
    private static final String patternVariable = "\\$\\{([^}]*)\\}";
    final Map<String, String> contextVariables = new HashMap<>();

    public void buildContextVariable(Map<String, String> variablesToSave) {
        contextVariables.putAll(variablesToSave);
    }

    public String getVariableValue(String key) {
        String k = convertKeyToVariableKey(key);
        String variableValue = contextVariables.get(k);
        if (variableValue == null) {
            throw new RuntimeException("Error, variable does not exist");
        }
        return variableValue;
    }

    public String buildJsonWithContext(final String json) {
        String finalJson = json;
        for (String key : contextVariables.keySet()) {
            finalJson = finalJson.replace("${" + key + "}", contextVariables.get(key));
        }
        return finalJson;
    }

    private String convertKeyToVariableKey(String key) {
        String keyConverted = key.trim();
        Pattern p = Pattern.compile(patternVariable);
        Matcher m = p.matcher(keyConverted);
        if (m.matches()) {
            keyConverted = m.group(1);
        }
        return keyConverted;
    }
}
