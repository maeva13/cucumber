package cucumber.context;

import io.restassured.response.Response;

public class HttpCallContext {
    Response httpCallResponse;

    public Response getHttpCallResponse() {
        return this.httpCallResponse;
    }

    public String getResponseString() {
        return this.httpCallResponse.getBody().toString();
    }

    public void setHttpCallResponse(Response httpCallResponse) {
        this.httpCallResponse = httpCallResponse;
    }
}
