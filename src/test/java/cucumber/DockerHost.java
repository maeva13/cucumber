package cucumber;

public class DockerHost {
    public static String getDockerHost() {
        String dockerHostFromEnv = System.getenv("DOCKER_HOSTNAME");
        return dockerHostFromEnv == null ? "localhost" : dockerHostFromEnv;
    }

    public static void main(String[] args) {
        System.out.println(DockerHost.getDockerHost());
    }
}
