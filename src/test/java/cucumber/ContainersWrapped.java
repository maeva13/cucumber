package cucumber;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.sql.*;

@Testcontainers
public class ContainersWrapped {
    private static final String DATABASE_NAME = "DatabaseName";
    private static final String NETWORK_ALIAS_POSTGRES = "postgres";
    private static final String POSTGRES_USER = "user";
    private static final String POSTGRES_PASSWORD = "password";

    private PostgreSQLContainer<?> pgContainer;
    private GenericContainer<?> techDatabaseContainer;

    private GenericContainer<?> lambdaContainer;

    private static ContainersWrapped singleInstance;

    private ContainersWrapped() {

    }

    public static synchronized ContainersWrapped getInstance() {
        if (singleInstance == null) {
            singleInstance = new ContainersWrapped();
            singleInstance.init();
        }
        return singleInstance;
    }

    private void init() {
        Network network = Network.newNetwork();

        pgContainer = new PostgreSQLContainer<>("postgres:13.6-alpine")
                .withDatabaseName(DATABASE_NAME)
                .withUsername(POSTGRES_USER)
                .withPassword(POSTGRES_PASSWORD)
                .withNetwork(network)
                .withNetworkAliases(NETWORK_ALIAS_POSTGRES);
        pgContainer.start();
    }

    public String getAppRootPath() {
        return "http://" + DockerHost.getDockerHost() + ":" + lambdaContainer.getMappedPort(8080);
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DockerHost.getDockerHost() + ":" + pgContainer.getMappedPort(5432) + "/" + DATABASE_NAME, POSTGRES_USER, POSTGRES_PASSWORD);
    }

    public void databasePostgreSQLCleanup() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Driver not loaded");
        }

        try {
            Connection con = getConnection();
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery("SELECT table_name \n" +
                    "FROM information_schema.tables\n" +
                    "WHERE table_schema='hubvellocal'\n" +
                    "AND table_type='BASE TABLE'\n" +
                    "AND table_name!='role'\n" +
                    "AND table_name!='feature'\n" +
                    "AND table_name!='databasechangeloglock'\n" +
                    "AND table_name!='databasechangelog';");
            StringBuilder bigQueryDeffered = new StringBuilder();

            while (rs.next()) {
                String tableName = rs.getString("table_name");
                bigQueryDeffered.append("TRUNCATE ").append(tableName).append(" RESTART IDENTITY CASCADE;\n");
            }

            PreparedStatement ps = con.prepareStatement(bigQueryDeffered.toString());
            ps.execute();
            con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    // Get logs of custom container
    public void techDatabaseLogs() {
        System.out.println(techDatabaseContainer.getLogs());
    }
}
