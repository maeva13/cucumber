package cucumber.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JSONUtils {
    public static String readJsonFileToString(String jsonPath) throws IOException {
        Path path = Paths.get("src/test/resources/json/" + jsonPath);
        BufferedReader reader = Files.newBufferedReader(path);
        StringBuilder json = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            json.append(line);
        }
        return json.toString();
    }
}
